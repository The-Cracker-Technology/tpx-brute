rm -rf /opt/ANDRAX/tpx-brute

/opt/ANDRAX/python27/bin/pip2.7 install termcolor

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Install termcolor... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf $(pwd) /opt/ANDRAX/tpx-brute

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
